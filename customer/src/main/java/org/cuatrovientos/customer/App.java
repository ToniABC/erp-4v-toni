/**
 * 
 */
package org.cuatrovientos.customer;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

/**
 * Simple Menu DataBase exercise
 * @author Toni
 */
public class App {
	
    public static void main( String[] args )
    		throws ClassNotFoundException, SQLException {
    	
    	Class.forName("org.sqlite.JDBC");
		Connection connection = DriverManager.getConnection("jdbc:sqlite:customer.db");
		Statement statement = connection.createStatement();
    	
		Scanner reader = new Scanner(System.in);
		int option = 0;
		
		do {
			System.out.println();
			System.out.println("Please Selet: ");
			System.out.println();
			System.out.println("0) Create Database");
			System.out.println("1) Show Customers");
			System.out.println("2) Insert Customer");
			System.out.println("3) Modify Customer");
			System.out.println("4) Remove Customer");
			System.out.println("5) Remove All Customers");
			System.out.println("6) Quit");
	
			System.out.println();
			
			option = Integer.parseInt(reader.nextLine());
			
			switch (option) {
				case 0:
						createDatabase(statement);
						break;
				case 1:
						showCustomers(statement);
						break;
				case 2:
						insertCustomer(statement, reader);
						break;
				case 3:
						modifyCustomer(statement, reader);
						break;
				case 4:
						deleteCustomer(statement, reader);
						break;
				case 5:
						deleteCustomers(statement);
						break;
				case 6:
						System.out.println();
						System.out.println("See you");
						break;
				default:
						System.out.println();
						System.out.println("TRY AGAIN");
						break;
			}
			
		} while (option!=6);

		connection.close();
    }

	/**
	 * @param statement
	 * @throws SQLException
	 */
	private static void deleteCustomers(Statement statement) throws SQLException {
		String deleteSql = "delete from customer";
		statement.executeUpdate(deleteSql);
	}

	/**
	 * @param statement
	 * @param reader
	 * @throws SQLException
	 */
	private static void deleteCustomer(Statement statement, Scanner reader) throws SQLException {
		int idCustomer;
		System.out.println();
		System.out.println("Insert Id of the Customer");
		idCustomer = Integer.parseInt(reader.nextLine());
		String deleteSql = "delete from customer where id=" + idCustomer;
		statement.executeUpdate(deleteSql);
	}

	/**
	 * @param statement
	 * @param reader
	 * @throws SQLException
	 */
	private static void modifyCustomer(Statement statement, Scanner reader) throws SQLException {
		int idCustomer;
		String nameCustomer;
		System.out.println();
		System.out.println("Insert Id of the Customer");
		idCustomer = Integer.parseInt(reader.nextLine());
		System.out.println("Insert New Name of the Customer");
		nameCustomer = reader.nextLine();
		String updateSql = "update customer set name='" + nameCustomer + "' where id=" + idCustomer;
		statement.executeUpdate(updateSql);
	}

	/**
	 * @param statement
	 * @param reader
	 * @throws SQLException
	 */
	private static void insertCustomer(Statement statement, Scanner reader) throws SQLException {
		int idCustomer;
		String nameCustomer;
		System.out.println();
		System.out.println("Insert Id of the Customer");
		idCustomer = Integer.parseInt(reader.nextLine());
		System.out.println("Insert Name of the Customer");
		nameCustomer = reader.nextLine();
		String insertSql = "insert into customer values(" + idCustomer + ", '"+ nameCustomer + "')";
		statement.executeUpdate(insertSql);
	}

	/**
	 * @param statement
	 * @throws SQLException
	 */
	private static void showCustomers(Statement statement) throws SQLException {
		String select = "select * from customer order by name desc";
		ResultSet resultSet = statement.executeQuery(select);
		
		while (resultSet.next()) {
			System.out.print("ID: " + resultSet.getInt("id"));
			System.out.println(" Name: " + resultSet.getString("name"));
		}
		
		resultSet.close();
	}

	/**
	 * @param statement
	 * @throws SQLException
	 */
	private static void createDatabase(Statement statement) throws SQLException {
		String sql = "create table customer (id integer, name varchar(40))";
		statement.executeUpdate(sql);
	}
	
}
